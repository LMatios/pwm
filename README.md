Introdução
 
Qual o desafio?
 
Construção da pipeline de CI/CD, para a aplicação PWM.
 
Perguntas:
O que é o Job package?
O que é feito nele?
Como é feito?
E por que?
 
# Desafio JPs
 
Desafio para implantação da ferramenta PWM no ambiente de homologação(Rancher)
 
- Mateus Pedro
- Wanderson
- Anderson
 
# PWM
 
Aplicação open source com o objetivo de fazer o gerenciamento de senhas para diretórios LDAP.
 
Página oficial do projeto: [https://github.com/pwm-project/pwm/](https://github.com/pwm-project/pwm/).
 
 
## Requisitos
O Deploy do PWM pode ser feito de três formas, através dos seguintes artefatos:
 
| Artifact| Description |
| --- | --- |
| WAR | Standard Java WAR (Web Archive) Necessário ter uma configuração do Tomcat e do Java funcionando em seu servidor. |
| Executable | Executável de linha de comando, inclui Tomcat(requer Java). |
| Docker | Docker image que comtém Java e Tomcat. |
 
 
Neste desafio utilizaremos a versão através do arquivo executável .jar.
 
 
 
### Package
	
 
Os artefatos precisam antes ser compilados, para em seguida serem utilizados. Para isso utilizaremos a ferramenta Maven. 
 
Obs: Em versões mais recentes o autor do projeto incluiu uma versão do Maven no projeto, tornando desnecessária a instalação do mesmo. Como estamos trabalhando com uma versão mais antiga, será necessário a instalação do Maven.
 
Requisitos:
* Java 11 JDK ou melhor
* Maven
 
Passos:
 
1. Faça o clone deste repositório:
```
git clone //URL
```
2. Dirija-se a pasta clonada. E inicie o processo de package.
```
mvn clean install package
```
3. Ao final, executar os arquivo .jar que foi gerado na pasta /onejar/target.
```
cd /onejar/target
java -jar pwm-onejar-2.1.0-SNAPSHOT.jar -applicationPath /pwm/onejar/target/
```
 
## Criação da pipeline
 
A pipeline foi dividida de acordo com os requisitos, sendo elas: Package, Docker Build e Deploy.
 
Obs: Antes da criação do Job, criamos um cache que irá salvar os conteúdos da pasta onejar/target, tanto para otimizar o tempo de build, quanto para ser utilizado na geração da imagem Docker.
 
### Variables
 
Foi definida a variável IMAGE_TAG, que conterá o nome da imagem Docker que virá a ser gerada, junto com a versão.
 
### Job Package
 
Job responsável por fazer a compilação dos arquivos executáveis, e gerar os artefatos do mesmo. O Maven será a ferramenta de compilação utilizada.
 
Image: Será utilizada a imagem maven:3.6-openjdk-11, já que a mesma contém o Maven, e o java versão JDK 11, conforme os requisitos da aplicação.
 
Script: delegamos o processo de geração dos arquivos para um script shell (build.sh).
 
    - Build.sh 
    O script build.sh, irá apenas verificar se os arquivos da pasta onejar/target foram baixados corretamente pelo cache, caso não, ele irá iniciar o processo de package utilizando o maven.
 
    Comando mvn package:
 
    - Devido a problemas de restrição deste repositório, tiveram que ser utilizadas configurações personalizadas, vindas de um arquivo .xml.
    - Algumas opções no comando do maven tiveram que ser utilizadas, para que ele ignore certas verificações contento o SSL:
 
    1. -Dmaven.wagon.http.ssl.insecure=true
    2. -Dmaven.wagon.http.ssl.allowall=true
    3. -Dmaven.wagon.http.ssl.ignore.validity.dates=true
 
Comando completo com as opções:
mvn clean install -s settings.xml -Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true -Dmaven.wagon.http.ssl.ignore.validity.dates=true
 
 
### Job Docker Build
 
Job responsável pela geração da imagem contendo a aplicação PWM.
 
Image: Foi utilizada a imagem padrão do docker, versão stable.
 
Services: Definimos que o docker utilizará o dind(docker-in-docker) para utilizar os comandos de docker dentro do container.
 
Before_Script: 
 
Utilizamos o Before_Script, para que seja feito o login no sistema interno do gitlab, antes da geração da imagem.
 
- Docker login: login no sistema docker interno do gitlab. 
- Foram utilizadas as variáveis pré-existentes, sendo elas o nome de usuário ($CI_REGISTRY_USER) a senha ($CI_REGISTRY_PASSWORD).
 
Script:
- Docker build, para realizar o build da imagem contida no arquivo Dockerfile.
- Docker push, fazer o upload da imagem. esse upload será feito para o registro interno do gitlab.
 
Arquivo Dockerfile:
![alt text](images/dockerfile.PNG)
 
    - Dockerfile:
    FROM openjdk:16-ea-23-jdk-alpine3.12  <-- Buscamos utilizar a menor imagem possível por questões de otimização.
 
    COPY onejar/target /usr/src/pwm <-- Copiar os arquivos da pasta onejar/target para o diretório pwm(caso não exista, o mesmo será criado).
 
    WORKDIR /usr/src/pwm <-- Mudar o diretório padrão da imagem para o dir da aplicação pwm.
 
 
    EXPOSE 8443 <-- Expor a porta onde o serviço roda.
    
    CMD ["java", "-jar", "pwm-onejar-2.1.0-SNAPSHOT.jar", "-applicationPath", "/usr/src/pwm"] <-- Comando a ser executado assim que a imagem subir.
     
 
### Job Deploy  
 
O deploy é responsável em fazer a entrega da aplicação em um ambiente para ser utilizado pelo usuário, neste desafio, o ambiente que será utilizado para fazer o deploy e o rancher 1.6  uma plataforma de gerenciamento e gestão de containers.
No deploy_job primeiramente será utilizada a imagem cdrx/rancher-gitlab-deploy, uma containers que possibilita a utilização do comando do rancher, depois foi declarado algumas variáveis que será usada para fazer a conexão entre o gitlab com o servidor rancher.
 
STACK_NAME: nessa variável será aponta qual é o nome da stack que vai ser usada ou criar na plataforma do rache 
RANCHER_SERVICE: Essa variável é usada para indicar qual o nome serviço onde vai ser executado o container  
RANCHER_URL: endereço do rancher onde será realizado o deploy  
RANCHER_ACCESS_KEY: variável responsável para armazenar chave de acesso do ambiente   
RANCHER_SECRET_KEY: variável responsável para armazenar chave de acesso a API do rancher 
RANCHER_ENVIRONMENT: variável usada para apontar qual ambiente vai será feito do ranche que vai ser feito o deploy  
IMAGE_TAG: essa variável indicar qual foi a imagem que foi  gerada pelo docker_build  
 
Na próxima etapa foi utilizado o seguinte comando 
 
upgrade --create --rancher-url $RANCHER_URL --labels apptype=normal --rancher-key $RANCHER_ACCESS_KEY --rancher-secret $RANCHER_SECRET_KEY --environment $RANCHER_ENVIRONMENT --stack $STACK_NAME --service $RANCHER_SERVICE --new-image $IMAGE_TAG --finish-upgrade 
 
Esse comando é responsável em fazer comunicação entre o gitlab com a plataforma do rancher, permitindo fazer o deploy da imagem gerada no job docker_build. No ambiente do rancher  
OBS: Se na plataforma do rancher não estiver criada a stack e o serviço, esse comando possibilita criar automaticamente esses dois serviços sem a necessidade de fazer outro job 
 
 
## Referências
 
- Opções do comando maven: http://maven.apache.org/wagon/wagon-providers/wagon-http/
- Gitlab container registry: https://youtu.be/mhRmdI0I4Bc
 
 
 
 
 
 
 



